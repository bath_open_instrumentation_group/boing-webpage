---
layout: home
---

# The Bath Open INstrumentation Group

The Bath Open INstrumentation Group is a research group in the Centre for Photonics and Photonic Materials at the University of Bath. The focus of the group's research is making scientific equipment which is completely open source.

Our core project is the [OpenFlexure project](https://openflexure.org).