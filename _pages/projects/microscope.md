---
layout: page-centered
title: OpenFlexure Project
permalink: /projects/openflexure/
sitemap: true
---

![]({{"/assets/microscope_render.png"| prepend: site.baseurl }}){: .image-medium }{: .float-right }

The OpenFlexure project aims to make high precision mechanical positioning available to anyone with a 3D printer - for use in microscopes, micromanipulators, and more.

For more detail please visit [openflexure.org](https://openflexure.org)

![]({{"/assets/blockstage_render.png"| prepend: site.baseurl }}){: .image-medium }{: .float-left }