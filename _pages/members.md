---
layout: page
title: Group Members
permalink: /members/
nav: true
---


## Group Members
 
* [Richard Bowman](https://researchportal.bath.ac.uk/en/persons/richard-bowman). 
* [Julian Stirling](https://researchportal.bath.ac.uk/en/persons/julian-stirling)
* [Joel Collins](https://researchportal.bath.ac.uk/en/persons/joel-collins)
* [Ed Meng](https://researchportal.bath.ac.uk/en/persons/ed-meng)
* [Joe Knapper](https://researchportal.bath.ac.uk/en/persons/joe-knapper)
* Kaspar Bumke

## Associate Members
* [William Wadsworth](https://researchportal.bath.ac.uk/en/persons/william-wadsworth)

## Past Members
* [Kerrianne Harrington](https://www.researchgate.net/profile/Kerrianne_Harrington)